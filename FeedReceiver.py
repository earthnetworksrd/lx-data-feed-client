#!/usr/bin/python
###
# Python 2/3 compatibility stuff
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from six import string_types
###############################################################################
#
# Copyright (c) 2017 Earth Networks, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

########################################################################
# FeedReceiver
# Lightning data feed receiver classes and functions
# Includes:
#   FeedReceiver    - Class for connecting to datafeed
#   Pulse           - Class for decoding Pulse information
#   Flash           - Class for decoding Flash information
#   checksum        - Computes checksum as used in Lx Data Feed
#   epoch2timestamp - converts Unix epoch time to UTC timestamp
#   timestamp2epochnano - converts UTC timestamp to Unix epoch time, and fractional nanoseconds
#
# All classes should have full Python 2 and Python 3 compatibility

# Imports
# numpy is used for building arrays, and for general math
# import numpy as np
# we really only need this to figure out where the location file is stored
import os, sys
# struct allows decoding binary data, needed for raw LTG files and binary feeds
import struct
# time and calendar are generic libraries for workign with time
import time, calendar, datetime
# json is a build in library for decoding json files
import json, numbers
# threading is only used by the FeedReciever class, and allows it to 
# operated asynchronously
import threading
import socket
# some of the files are compressed with gzip, this will decompress them
import gzip
#config parsing stuff
try:
    #python 3
    from configparser import ConfigParser
except:
    #python 2
    from ConfigParser import ConfigParser
import ast

DEBUGGING = False

class Pulse( ):
    """Pulse
    This is a general class for an ENTLN located 'pulse'.  One lightning 
    'flash' is made up of many 'pulses'.  
    """

    def __init__( self, messageString=None, version=3, format='ascii' ):
        '''
        initializes pulse instance
        input:
            messageString   the string to decode
            version         2 or 3, the version of lxdatafeed
            format          'ascii' or 'binary'
            '''

        # store the initializing string, just in case
        self.messageString = messageString
        self.version       = version
        self.format        = format

        #initialize all possible values for a pulse, in case the message string doesn't include all information
        self.type      = None   #0 CG, 1 IC, 40 WWLLN
        self.latitude  = None   #in decimal degrees
        self.longitude = None   #in decimal degrees
        self.timestamp = None   #string YYYY-MM-DDTHH:MM:SS.nnnnnnnnn   (except in v2 ascii)
        self.epoch     = None   #unix epoch time - integer number of seconds since 1970
        self.nano      = None   #integer number of nanoseconds after the epoch 
        self.peakcurrent = None #in Amps
        self.height    = None   #in meters above sealevel
        self.numsensors= None   #the number of sensors which contributed to the solution
        self.eemajor   = None   #error ellipse major axis, in km
        self.eeminor   = None   #error ellipse minor axis, in km 
        self.eebearing = None   #error ellipse bearing in degrees

        #do we have a message string to decode?
        if self.messageString is None:
            return

        if self.format == 'ascii':
            if self.version == 2:
                self._decode_v2ascii()
            elif self.version == 3:
                self._decode_v3ascii()
            else:
                raise ValueError( 'Pulse.__init__: Unknown pulse version %s - can not decode'%repr(self.version))
        elif self.format == 'binary':
            if self.version == 2:
                self._decode_v2binary()
            elif self.version == 3:
                self._decode_v3binary()
            else:
                raise ValueError( 'Pulse.__init__: Unknown pulse version %s - can not decode'%repr(self.version))
        else:
            ###
            # I don't know what format this is
            raise ValueError('Pulse.__init__: unknown format: %s - can not decode'%repr(self.format))

    def _decode_v2ascii( self ):
        '''
        line format for both pulses and flashes is:
        <type>,<timestamp>,<latitude>,<longitude>,<peak current>,<reserved>,<height>,<number sensors>,<multiplicity>\r\n

        input 
            self
        output
            no direct output, populates attributes
        '''
        fields = self.messageString.decode().split( ',' )
        self.type      = int(   fields[0])
        self.timestamp = fields[1].strip()
        self.latitude  = float( fields[2])
        self.longitude = float( fields[3])
        self.peakcurrent=float( fields[4])
        #fields[5] is reserved
        self.height    = float( fields[6])
        self.numsensors= int(   fields[7])
        #fields[8] is multiplicity, but this is always 0 for pulses

    def _decode_v2binary( self ):
        '''
        decodes a binary pulse (string input is binary)
        '''
        #we're working with a binary string, not human readable
    
        # A pulse message should be 26 bytes long, including the following:
        # !! all numbers are encoded big endian !!		
        # 0 	length		unsigned int	56 (for flash)
        # 1		type		unsigned int	0 CG, 1 IC, 9 keep alive
        # 2-5	time (s)	unsigned int	epoc time of the strongest pulse (CG if available)
        # 6-7	time (ms)	unsigned int	ms of the second
        # 8-11	lat			signed int		lat *10,000,000, positive N, negative S
        # 12-15	lon			signed int		lon *10,000,000, positive E, negative W
        # 16-19 current		signed int		in Amperes
        # 20                Reserved
        # 21-22 height		unsigned int	in meters
        # 23	sensors		unsigned int
        # 24                multiplicity    (not used in pulses)
        # 25	check sum	unsigned int	Check sum
        self.length,        = struct.unpack(  'B', self.messageString[0:1] )
        #check that the length is what we expect
        if self.length != 26:
            #it seems that this message string is malformed
            raise Exception( 'Pulse._decode_v2binary: message string malformed - invalid size: %i'%self.length )

        self.type,          = struct.unpack(  'B', self.messageString[1:2] )
        self.epoch,         = struct.unpack( '>I', self.messageString[2:6] )
        self.nano           = struct.unpack( '>H', self.messageString[6:8] )[0]*1000000

        self.latitude       = struct.unpack( '>i', self.messageString[8:12] )[0]/10000000.
        self.longitude      = struct.unpack( '>i', self.messageString[12:16] )[0]/10000000.
        self.peakcurrent,   = struct.unpack( '>i', self.messageString[16:20] )
        self.height,        = struct.unpack( '>H', self.messageString[21:23] )
        self.numsensors,    = struct.unpack(  'B', self.messageString[23:24] )
        #self.messageString[21:23] - not used in pulses
        self.checksum,      = struct.unpack(  'B', self.messageString[25:] )

        #check that the checksum matches
        if checksum( self.messageString[:-1] ) != self.checksum:
            raise ValueError('Pulse._decode_v2binary: Bad Checksum: %i %i'%(checksum( self.messageString[:-1] ), self.checksum ) )

        #special fields (time)
        self._update_special_attributes( )

    def _decode_v3ascii( self):
        '''
        decode json pulse (string input in json format)
        '''

        #then we're working with a json string
        #pulse feed
        #{ "time":"2017-02-26T17:32:29.817443878Z",
        #  "type":0,
        #  "latitude":-21.5043742,
        #  "longitude":-49.1724032,
        #  "peakCurrent":-21356.0,
        #  "icHeight":0.0,
        #  "numSensors":16,
        #  "eeMajor":278.0,
        #  "eeMinor":200.0,
        #  "eeBearing":21.8
        #}
        
        # first we decode the json string, this puts everything into a dictionary
        jsonDict = json.loads( self.messageString )

        #apply all the keys as attributes to self
        #this works so easy because the attributes of this object are named the same 
        #as the attributes of the json string (almost), the outliers we handle with 
        #_update_sepcial_attributes
        for key in jsonDict.keys():
            setattr( self, key.lower(), jsonDict[key] )
        
        #special fields (time)
        self._update_special_attributes( )

    def _decode_v3binary( self ):
        '''
        decodes a binary pulse (string input is binary)
        '''
        #we're working with a binary string, not human readable
    
        # A flash message should be 32 bytes long, including the following:
        # !! all numbers are encoded big endian !!		
        # 0 	length		unsigned int	32 (for flash)
        # 1		type		unsigned int	0 CG, 1 IC, 9 keep alive
        # 2-5	time (s)	unsigned int	epoc time of the strongest pulse (CG if available)
        # 6-9	time (ns)	unsigned int	ns of the second
        # 10-13	lat			signed int		lat *10,000,000, positive N, negative S
        # 14-17	lon			signed int		lon *10,000,000, positive E, negative W
        # 18-21 current		signed int		in Amperes
        # 22-23 height		unsigned int	in meters
        # 24	sensors		unsigned int
        # 25-26	err major	unsigned int	Error elipse major axis, meters
        # 27-28	err minot	unsigned int	Error elipse minor axis, meters
        # 29-30	err bearing	unsigned int	Error elipse bearing, degrees
        # 31	check sum	unsigned int	Check sum
        self.length,        = struct.unpack(  'B', self.messageString[0:1] )
        if self.length != 32:
            #it seems that this message string is malformed
            raise Exception( 'Pulse._decode_v3binary: message string malformed - invalid size: %i'%self.length )
        
        self.type,          = struct.unpack(  'B', self.messageString[1:2] )
        self.epoch,         = struct.unpack( '>I', self.messageString[2:6] )
        self.nano,          = struct.unpack( '>I', self.messageString[6:10] )
        self.latitude       = struct.unpack( '>i', self.messageString[10:14] )[0]/10000000.
        self.longitude      = struct.unpack( '>i', self.messageString[14:18] )[0]/10000000.
        self.peakcurrent,   = struct.unpack( '>i', self.messageString[18:22] )
        self.height,        = struct.unpack( '>H', self.messageString[22:24] )
        self.numsensors,    = struct.unpack(  'B', self.messageString[24:25] )
        self.eemajor,       = struct.unpack( '>H', self.messageString[25:27] )
        self.eeminor,       = struct.unpack( '>H', self.messageString[27:29] )
        self.eebearing,     = struct.unpack( '>H', self.messageString[29:31] )
        self.checksum,      = struct.unpack(  'B', self.messageString[31:] )

        #check that the checksum matches
        if checksum( self.messageString[:-1] ) != self.checksum:
            raise ValueError( 'Pulse._decode_v3binary: Bad Checksum: %i %i'%(checksum( self.messageString[:-1] ), self.checksum ))

        #special fields (time)
        self._update_special_attributes( )

    def _update_special_attributes( self ):
        '''
        Updates a small number of special fields which don't come directly 
        from the feed message string
        '''
        #json peculiarities:
        #height, is names 'icheight'
        if hasattr( self, 'icheight' ):
            self.height = self.icheight
        #timestamp is name time
        if hasattr( self, 'time' ):
            self.timestamp = self.time

        #convert timestamp to epoch and nano, or vice versa (depending on binary or ascii)
        if self.epoch is None:
            #try generating from time stamp, 
            #this happens in ascii feeds
            self.epoch, self.nano = timestamp2epochnano( self.timestamp )
        elif self.timestamp is None:
            #generate the timestamp from the epoch and nano
            self.timestamp = epoch2timestamp( self.epoch ) + '.%09i'%self.nano
        
    def printout(self):
        '''
        returns string of formatted decoded data
        '''

        S = ""
        S += ('%2i,'    %self.type       ).rjust(4)
        S += (' %s,'    %self.timestamp  ).rjust(22)
        S += (' %5.4f,' %self.latitude   ).rjust(12)
        S += (' %5.4f,' %self.longitude  ).rjust(12)
        S += (' %9i,'   %self.peakcurrent).rjust(11)
        S += (' %5i,'   %self.height     ).rjust(6)
        S += (' %i'     %self.numsensors ).rjust(3)

        ###
        # error ellipse info may not be available
        if self.eemajor is None: return S


        S += ','
        S += (' %3.1f,' %self.eemajor   ).rjust(8)
        S += (' %3.1f,' %self.eeminor   ).rjust(8)
        S += (' %3.1f ' %self.eebearing ).rjust(8)
        return S

class Flash( ):
    '''
    Flash
    One lightning 'flash' is made up of many 'pulses'.
    '''

    def __init__( self, messageString=None, version=3, format='ascii' ):
        '''
        initializes pulse instance
        input:
            messageString   the string to decode
            version         2 or 3, the version of lxdatafeed
            format          'ascii' or 'binary'
            '''

        # store the initializing string, just in case
        self.messageString = messageString
        self.version       = version
        self.format        = format

        #initialize all possible values for a flash, in case the message string doesn't include all information
        self.type      = None   #0 CG, 1 IC, 40 WWLLN
        self.latitude  = None   #in decimal degrees
        self.longitude = None   #in decimal degrees
        self.timestamp = None   #string YYY-MM-DDTHH:MM:SS.nnnnnnnnn (except v2)
        self.epoch     = None   #unix epoch time - integer number of seconds since 1970
        self.nano      = None   #integer number of nanoseconds after the epoch 
        self.peakcurrent=None   #in Amps
        self.height    = None   #in meters above sealevel
        self.numsensors= None   #the number of sensors which contributed to the solution
        self.multiplicity=None  #icMultiplicity + cgMultiplicity
        self.icmultiplicity=None #number of IC pulses included in this flash
        self.cgmultiplicity=None #number of CG pulses included in this flash
        self.ullatitude= None   #upper left (max) latitude
        self.lrlatitude= None   #lower right (min) latitude
        self.ullongitude=None   #upper left (min) longitude
        self.lrlongitude=None   #lower right (max) longitude
        #there's a lot more time stamps for a flash, but they're not always set
        self.starttimestamp=None #Time of the first pulse in the flash
        self.startepoch= None
        self.startnano = None
        self.duration  = None   #duration of the flash in nanoseconds
        self.pulses    = []     #a list of pulse objects (may be empty)


        #do we have a message string to decode?
        if self.messageString is None:
            return

        if self.format == 'ascii':
            if self.version == 2:
                self._decode_v2ascii()
            elif self.version == 3:
                self._decode_v3ascii()
            else:
                raise ValueError( 'Flash.__init__: Unknown pulse version %s - can not decode'%repr(self.version))
        elif self.format == 'binary':
            if self.version == 2:
                self._decode_v2binary()
            elif self.version == 3:
                self._decode_v3binary()
            else:
                raise ValueError( 'Flash.__init__: Unknown pulse version %s - can not decode'%repr(self.version))
        else:
            ###
            # I don't know what format this is
            raise ValueError('Flash.__init__: unknown format: %s - can not decode'%repr(self.format))

    def _decode_v2ascii( self ):
        '''
        line format for both pulses and flashes is:
        <type>,<timestamp>,<latitude>,<longitude>,<peak current>,<reserved>,<height>,<number sensors>,<multiplicity>\r\n

        input 
            self
        output
            no direct output, populates attributes
        '''
        fields = self.messageString.decode().split( ',' )
        self.type      = int(   fields[0])
        self.timestamp = fields[1].strip()
        self.latitude  = float( fields[2])
        self.longitude = float( fields[3])
        self.peakcurrent=float( fields[4])
        #fields[5] is reserved
        self.height    = float( fields[6])
        self.numsensors= int(   fields[7])
        self.multiplicity=int(  fields[8] )

        self._update_special_attributes( )

    def _decode_v2binary_flash( self, inputString ):
        '''
        decodes a binary pulse (string input is binary)
        '''

        # A flash message should be 26 bytes long, including the following:
        # !! all numbers are encoded big endian !!		
        # 0 	length		unsigned int	56 (for flash)
        # 1		type		unsigned int	0 CG, 1 IC, 9 keep alive
        # 2-5	time (s)	unsigned int	epoc time of the strongest pulse (CG if available)
        # 6-7	time (ms)	unsigned int	ms of the second
        # 8-11	lat			signed int		lat *10,000,000, positive N, negative S
        # 12-15	lon			signed int		lon *10,000,000, positive E, negative W
        # 16-19 current		signed int		in Amperes
        # 20                Reserved
        # 21-22 height		unsigned int	in meters
        # 23	sensors		unsigned int
        # 24                multiplicity    (not used in pulses)
        # 25	check sum	unsigned int	Check sum
        self.length,        = struct.unpack(  'B', inputString[0:1] )
        #check that the length is what we expect
        if self.length != 26:
            #it seems that this message string is malformed
            raise Exception( 'Pulse._decode_v2binary: message string malformed - invalid size: %i'%self.length )

        self.type,          = struct.unpack(  'B', inputString[1:2] )
        self.epoch,         = struct.unpack( '>I', inputString[2:6] )
        self.nano           = struct.unpack( '>H', inputString[6:8] )[0]*1000000

        self.latitude       = struct.unpack( '>i', inputString[8:12] )[0]/10000000.
        self.longitude      = struct.unpack( '>i', inputString[12:16] )[0]/10000000.
        self.peakcurrent,   = struct.unpack( '>i', inputString[16:20] )
        self.height,        = struct.unpack( '>H', inputString[21:23] )
        self.numsensors,    = struct.unpack(  'B', inputString[23:24] )
        self.multiplicity,  = struct.unpack(  'B', inputString[24:25] )
        self.checksum,      = struct.unpack(  'B', inputString[25:] )

        #check that the checksum matches
        if checksum( inputString[:-1] ) != self.checksum:
            raise ValueError('Flash._decode_v2binary: Bad Checksum: %i %i'%(checksum( self.messageString[:-1] ), self.checksum ) )

        #special fields (time)
        self._update_special_attributes( )

    def _decode_v2binary( self ):
        '''
        decodes a binary pulse (string input is binary)
        works for binary flash feeds and binary combo feeds
        '''
        #we're working with a binary string, not human readable
        #this could be a flash, or is could be a combo thing
        #for flashes messages, the first byte will define the size, and that size will be 56
        #for combination messages, the first 2 bytes will define the size
        # 0-1    length      unsigned int    greater than 56+32 (for combo messages)
        # 2-57   flash       flash message
        # 58-89  pulse_1     pulse message
        # 90-121 pulse_2     pulse message
        # ...
        # last byte         check sum

        #is this message a flash?
        length, 		= struct.unpack(  'B', self.messageString[0:1] )
        if length == 26:
            #this message is a flash
            self._decode_v2binary_flash( self.messageString )
            #there are no pulses, return now
            return
        
        #if it's not a flash, then lets see if it's a combo message
        length, 		= struct.unpack(  '>H', self.messageString[0:2] )
        #the length should be 2 + 26 + N*26 + 1, if not something is missing
        if (length-29)%26 !=0:
            #it seems that this message string is malformed
            raise Exception( 'Flash._decode_v2binary: message string malformed - invalid size: %i'%length )
        
        #get the checksum out of the way too
        #check that the checksum matches
        self.checksum,      = struct.unpack(  'B', self.messageString[-1:] )
        if checksum( self.messageString[:-1] ) != self.checksum:
            raise ValueError('Flash._decode_v2binary: Bad Checksum: %i %i'%(checksum( self.messageString[:-1] ), self.checksum ))

        #the flash portion of the flash is always in the same place:
        self._decode_v2binary_flash( self.messageString[2:28] )

        #now decode all the pulse sections
        iPulse = 54
        while iPulse < length-1:
            pulse = Pulse( messageString=self.messageString[iPulse:iPulse+26], version=2, format='binary' )
            self.add_pulse( pulse )
            iPulse += 26

    def _decode_v3ascii( self):
        '''
        decode json pulse (string input in json format)
        '''

        #then we're working with a json string
        #flash feed example:

        # first we decode the json string, this puts everything into a dictionary
        jsonDict = json.loads( self.messageString )

        #apply all the keys as attributes to self
        #this works so easy because the attributes of this object are named the same 
        #as the attributes of the json string (almost), the outliers we handle with 
        #_update_sepcial_attributes
        for key in jsonDict.keys():
            setattr( self, key.lower(), jsonDict[key] )
        
        #special fields (time)
        self._update_special_attributes( )

    def _decode_v3binary_flash( self, inputString ):
        '''
        decodes the flash portion of binary flash and combo strings
        '''

        ###
        # A flash message should be 56 bytes long, including the following:
        # !! all numbers are encoded big endian !!
        # 0 	length		unsigned int	56 (for flash)
        # 1		type		unsigned int	0 CG, 1 IC, 9 keep alive
        # 2-5	time (s)	unsigned int	epoc time of the strongest pulse (CG if available)
        # 6-9	time (ns)	unsigned int	ns of the second
        # 10-13	lat			signed int		lat *10,000,000, positive N, negative S
        # 14-17	lon			signed int		lon *10,000,000, positive E, negative W
        # 18-21 current		signed int		in Amperes
        # 22-23 height		unsigned int	in meters
        # 24	sensors		unsigned int
        # 25	IC mult		unsigned int	Number of IC pulses
        # 26	CG mult		unsigned int	Number of CG pulses
        # 27-30	start (s)	unsigned int	Start time (epoc)
        # 31-34 start (ns)	unsigned int	Start time fractional part
        # 35-38 Duration 	unsigned int	Duration of flash in ns
        # 39-42	UL lat		unsigned int	Upper Left corner latitude
        # 43-46	UL lon		unsigned int	Upper Left corner longitude
        # 47-50	UL lon		unsigned int	Lower Right corner latitude
        # 51-54	UL lon		unsigned int	Lower Right corner longitude
        # 55	Check sum	unsigned int	check sum
        

        self.length, 		= struct.unpack(  'B', inputString[0:1] )
        #check that the length is what we expect
        if self.length != 56:
            #it seems that this message string is malformed
            raise Exception( 'Flash._decode_v3binary_flash: message string malformed - invalid size: %i'%self.length )

        self.type,          = struct.unpack(  'B', inputString[1:2] )
        self.epoch,         = struct.unpack( '>I', inputString[2:6] )
        self.nano,          = struct.unpack( '>I', inputString[6:10] )
        self.latitude       = struct.unpack( '>i', inputString[10:14] )[0]/10000000.
        self.longitude      = struct.unpack( '>i', inputString[14:18] )[0]/10000000.
        self.peakcurrent,   = struct.unpack( '>i', inputString[18:22] )
        self.height,        = struct.unpack( '>H', inputString[22:24] )
        self.numsensors,    = struct.unpack( 'B', inputString[24:25] )
        self.icmultiplicity,= struct.unpack( 'B', inputString[25:26] )
        self.cgmultiplicity,= struct.unpack( 'B', inputString[26:27] )
        
        self.startepoch,    = struct.unpack( '>I', inputString[27:31] )
        self.startnano,     = struct.unpack( '>I', inputString[31:35] )
        self.duration,      = struct.unpack( '>I', inputString[35:39] )

        self.ullatitude     = struct.unpack( '>i', inputString[39:43] )[0]/10000000.
        self.ullongitude    = struct.unpack( '>i', inputString[43:47] )[0]/10000000.
        self.lrlatitude     = struct.unpack( '>i', inputString[47:51] )[0]/10000000.
        self.lrlongitude    = struct.unpack( '>i', inputString[51:55] )[0]/10000000.
        
        self.checksum,      = struct.unpack(  'B', inputString[55:] )

        #check that the checksum matches
        if checksum( inputString[:-1] ) != self.checksum:
            raise ValueError('Flash._decode_v3binary_flash: Bad Checksum: %i %i'%(checksum( self.messageString[:-1] ), self.checksum ))

        #special fields (time)
        self._update_special_attributes( )

    def _decode_v3binary( self ):
        '''
        decodes a binary pulse (string input is binary)
        works for binary flash feeds and binary combo feeds
        '''
        #we're working with a binary string, not human readable
        #this could be a flash, or is could be a combo thing
        #for flashes messages, the first byte will define the size, and that size will be 56
        #for combination messages, the first 2 bytes will define the size
        # 0-1    length      unsigned int    greater than 56+32 (for combo messages)
        # 2-57   flash       flash message
        # 58-89  pulse_1     pulse message
        # 90-121 pulse_2     pulse message
        # ...
        # last byte         check sum

        #is this message a flash?
        length, 		= struct.unpack(  'B', self.messageString[0:1] )
        if length == 56:
            #this message is a flash
            self._decode_v3binary_flash( self.messageString )
            #there are no pulses, return now
            return
        
        #if it's not a flash, then lets see if it's a combo message
        length, 		= struct.unpack(  '>H', self.messageString[0:2] )
        #the length should be 2 + 56 + N*32 + 1, if not something is missing
        if (length-59)%32 !=0:
            #it seems that this message string is malformed
            raise Exception( 'Flash._decode_v3binary: message string malformed - invalid size: %i'%length )
        
        #get the checksum out of the way too
        #check that the checksum matches
        self.checksum,      = struct.unpack(  'B', self.messageString[-1:] )
        if checksum( self.messageString[:-1] ) != self.checksum:
            raise ValueError('Flash._decode_v3binary: Bad Checksum: %i %i'%(checksum( self.messageString[:-1] ), self.checksum ))

        #the flash portion of the flash is always in the same place:
        self._decode_v3binary_flash( self.messageString[2:58] )

        #now decode all the pulse sections
        iPulse = 58
        while iPulse < length-1:
            pulse = Pulse( messageString=self.messageString[iPulse:iPulse+32], version=3, format='binary' )
            self.add_pulse( pulse )
            iPulse += 32

    def _update_special_attributes( self ):
        '''
        Updates a small number of special fields which don't come directly 
        from the feed message string
        '''
        #json peculiarities:
        #height, is named 'icheight'
        if hasattr( self, 'icheight' ):
            self.height = self.icheight
        #timestamp is named time
        if hasattr( self, 'time' ):
            self.timestamp = self.time
        #starttimestamp is named starttime
        if hasattr( self, 'starttime' ):
            self.starttimestamp = self.starttime

        #convert timestamp to epoch and nano, or vice versa (depending on binary or ascii)
        if self.epoch is None:
            #try generating from time stamp, 
            #this happens in ascii feeds
            self.epoch, self.nano = timestamp2epochnano( self.timestamp )
        elif self.timestamp is None:
            #generate the timestamp from the epoch and nano
            self.timestamp = epoch2timestamp( self.epoch ) + '.%09i'%self.nano
        
        #start time
        if self.startepoch is None and self.starttimestamp is not None:
            #try generating from time stamp, 
            #this happens in ascii feeds
            self.startepoch, self.startnano = timestamp2epochnano( self.starttimestamp )
        elif self.starttimestamp is None and self.startepoch is not None and self.startnano:
            #generate the timestamp from the epoch and nano
            self.starttimestamp = epoch2timestamp( self.startepoch ) + '.%09i'%self.startnano

        #V2 multiplicity to IC and CG multiplicity
        if self.multiplicity is not None:
            if self.type == 1:
                self.icmultiplicity = self.multiplicity
                self.cgmultiplicity = 0
            else:
                self.cgmultiplicity = self.multiplicity
                self.icmultiplicity = 0

    def add_pulse( self, pulse ):
        #adds a pulse to the list of pulses
        self.pulses.append( pulse )

    def printout(self):
        S = ''
        S += ('%s,' %self.timestamp).rjust(22)
        S += (' %2i,'   %self.type           ).rjust(4)
        S += (' %5.4f,' %self.latitude       ).rjust(12)
        S += (' %5.4f,' %self.longitude      ).rjust(12)
        S += (' %9i,'   %self.peakcurrent    ).rjust(11)
        S += (' %s,'    %repr(self.cgmultiplicity) ).rjust(5)
        S += (' %s,'    %repr(self.icmultiplicity) ).ljust(5)
        S += (' %i'     %len(self.pulses) )
        return S

class FeedReceiver( threading.Thread ):
    '''
    process that runs in th background creating stuctured data that provides updates of current information.
    A list of flashes, pulses or combos is created, either in binary or json.
    '''
    #dictionary to convert human readable options to the numbers the feed is expecting
    _feedConst = { 'flash':1, 'pulse':2, 'combo':3, 'ascii':1, 'binary':2 }

    def __init__( self, partnerId=None, **kwargs ): 
        '''
        initializes FeedReceiver attributes: self and any number of keyword arguments.
        arguments are set automatically if not specified. Default arguments are: 
            partnerId (mandatory)
            ip 
            port
            type	1: flash, 2: pulse, 3:combo (binary only)
            format	1: ascii, 2: binary
            showSource
            decode 
            config - if given indicates that a config file with initial parameters is given
        '''
        
        ###
        # initialize all the threading stuff
        threading.Thread.__init__(self)
        
        # best guess default settings
        self.partnerId = partnerId	#we really do need one
        self.ip        = ['107.23.135.182','107.23.152.248']    #defaults to testing feed, contact customer care for production end point
        self.port      = 2324
        self.type  = 'pulse'    #1: flash, 2: pulse, 3:combo (binary only)
        self.format= 'ascii'    #1: ascii, 2: binary
        self.version   = 3      #2: CSV, 3:JSON
        self.showSource= True   #shows type 40 for wwlln pulses
        self.meta      = True   #Adds additional info like error ellipse to feed output
        self.decode	   = True   #should we bother decoding the strings at all?

        # initial parameters can also be set with a config file
        if 'config' in kwargs.keys():
            #then we have a config file:
            cfg = ConfigParser()
            cfg.read( kwargs['config'] )
            
            for section in cfg.sections():
                for key, value in cfg.items( section ):
                    value = ast.literal_eval( value )
                    setattr(self,key,value)

        # were any kwargs passed?
        for key in kwargs:
            setattr( self, key, kwargs[key] )	

        # special options, this normalizes the form of these
        self.type   = self.type.lower()
        self.format = self.format.lower()

        # which IP should we use?
        self.whichIp = 0
            
        ###
        # initialize the messages
        self.received = []
                
        ###
        # set daemon flag so ctrl-c will kill the program
        self.daemon = True

        ###
        # set a running flag, so we can turn the thread off
        self.isRunning = False

        ###
        # open the socket
        self.lastOpened = 0
        self.open_socket()

    def close_socket( self ):
        '''
        closes socket

        input 
            self
        '''
        ###
        # used with reopening socket
        self.socket.close()
    
    def open_socket( self ):
        '''
        opens socket to a server

        input 
            self
        '''

        #limit the rate we can open the socket
        while time.time()-self.lastOpened < 5:
            time.sleep( .1 )
        self.lastOpened = time.time()

        ###
        # the reciever has a socket set up to pull the data feed
        self.socket = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
        self.socket.connect( (self.ip[self.whichIp], self.port) )

        ###
        # generate the request string that tells EN what to send us
        # note: python3 defaults to unicode strings, not what we want for the socket
        requestStr = b'{"p":"%s","v":%i,"f":%i,"t":%i,"class":3'%(self.partnerId.encode(), self.version, self._feedConst[self.format], self._feedConst[self.type] )
        #other options
        if self.meta:
            requestStr += b',"meta":true'
        if self.showSource:
            requestStr += b',"showSource":true'
        #close string
        requestStr += b'}'

        self.socket.sendall( requestStr )
        
        ###
        # clean up the sending part of the socket
        self.socket.shutdown(socket.SHUT_WR)

    def _loop_v2binary( self ):
        '''
        same as _loop_v3binary
        '''
        self._loop_v3binary()

    def _loop_v2ascii( self ):
        '''
        goes and gets a packet of data that is used as the message.
        appends message to flash or pulse string that is used for the Pulse and Flash classes. 
        
        handles both flash and pulse feeds.
        line format for both pulses and flashes is:
        <type>,<timestamp>,<latitude>,<longitude>,<peak current>,<reserved>,<height>,<number sensors>,<multiplicity>\r\n

        input 
            self
        output
            no direct output, instead creates updates to existing pulse or flash.
        '''

        #per documentation, each pulse (flash) is approximately 77 () bytes long.  
        #the fields are fixed width, and 0 padded.  But, in the unlikely event that 
        #a field exceeds the fixed width size allotted, the line will grow in length 
        #rather than collide.  This happens if the showSource:True option is used
        #each line ends with a carriage return and line feed, these can be used to 
        #split strings
        #line format for pulses and flashes is:
        #<type>,<timestamp>,<latitude>,<longitude>,<peak current>,<reserved>,<height>,<number sensors>,<multiplicity>\r\n
        #in the V2 feed, it is not possible to get extend information (flashes) or error ellipse information (pulses)
        message = b""
        while self.isRunning:

            # first thing we do, always is get a packet of data
            # get_packet handles what to do if the socket needs to be 
            # reset
            message = self._get_packet( message )
            
            # if the message is too short to get the length, we'll need 
            # another one
            if len(message) < 77:
                continue
            #do we have the line feed?
            if not b'\r\n' in message:
                continue

            ###
            # split out the message
            S = message.split( b'\r\n' )[0]
            #remove the parsed portion of the message
            message = message[ len(S)+2: ]
            
            #are we decoding the data?
            if not self.decode:
                #no we're not, just append the message to the list
                self.received.append( S )
                continue
            #apparently we are decoding, what type of message is this.
            #we can't differentiate based on the message string, so we have to 
            #trust the configuration
            if self.type == 'flash':
                self.received.append( Flash( S, version=2, format='ascii' ) )
            elif self.type == 'pulse':
                self.received.append( Pulse( S, version=2, format='ascii' ) )
            else:
                raise ValueError( 'FeedReceiver._loop_v2ascii: Unknown feed type: %s - unsure how to proceed'%self.type )
        
        #cleanup
        self.close_socket()

    def _loop_v3binary( self ):
        '''
        goes and gets a packet of data that is used as the message.
        appends messge to Pulse or Flash
        
        input 
            self
        output
            no direct output, instead creates updates to existing pulse or flash.
        '''

        message = b""
        while self.isRunning:

            # first thing we do, always is get a packet of data
            # get_packet handles what to do if the socket needs to be 
            # reset
            # Python2/3 note - Python 2 this is a string, Python 3 this is bytes
            message = self._get_packet( message )
            
            # if the message is too short to get the length, we'll need 
            # another one
            if len(message) < 2:
                continue
            
            # get the message length from the first 2 bytes of the message
            if self.type == 'combo':
                messageLen, = struct.unpack( '>H', message[:2] )
            else:
                # Python2/3 note - in Python3, the struct call can be replaced with message[0]
                # Python2/3 note - using struct on single elements requring indexing with slice
                messageLen, = struct.unpack( 'B', message[:1] )

            #make sure the message is long enough
            if len(message) < messageLen:
                continue

            ###
            # split out the message
            S = message[:messageLen]
            
            # decode the message?
            if not self.decode:
                #we don't decode at all
                self.received.append( S )
            elif self.type == 'pulse':
                self.received.append( Pulse( S, version=self.version, format='binary') )
            elif self.type == 'flash':
                self.received.append( Flash( S, version=self.version, format='binary') )
            elif self.type == 'combo':
                #this is handled in a flash object as well
                self.received.append( Flash( S, version=self.version, format='binary') )
            else:
                #i'm not sure what it is
                pass
            
            #update the message
            message = message[messageLen:]
        
        #clean up 
        self.close_socket()

    def _loop_v3ascii( self ):
        '''
        goes and gets a packet of data that is used as the message.
        appends message to flash or pulse string that is used for the Pulse and Flash classes. 
        
        input 
            self
        output
            no direct output, instead creates updates to existing pulse or flash.
        '''
        message = b""
        while self.isRunning:

            # first thing we do, always is get a packet of data
            # get_packet handles what to do if the socket needs to be 
            # reset
            message = self._get_packet( message )
            
            # if the message is too short to get the length, we'll need 
            # another one
            if len(message) < 4:
                continue
            
            # parse the message length so we know how much to get
            messageLen, = struct.unpack( '>I', message[:4] )

            #make sure the message is long enough
            if len(message) < messageLen:
                continue
            
            ###
            # split out the message, the first 4 bytes are the size
            S = message[4:messageLen]
            message = message[messageLen:]

            # now, what type of message is this.
            # we just use the config for this
            if not self.decode:
                #we don't decode at all
                self.received.append( S )
            elif self.type == 'flash':
                self.received.append( Flash(S, version=3, format='ascii') )
            elif self.type == 'pulse':
                self.received.append( Pulse(S, version=3, format='ascii') )
            else:
                raise ValueError( 'FeedReceiver._loop_v3ascii: Unknown feed type: %s - unsure how to proceed'%self.type )
        
        #cleanup
        self.close_socket()

    def _get_packet( self, message=b"" ):
        '''
        gets package of data. 
        if no data is received for over 20 secons an Exception is raised.
        else the data is turned into s message string that is then used in the methods 
        loop_binary, loop_binary_old, and loop_json.
        if the socket fails the feed is restarted.

        input
            self
            message			empty string that the data will be added to to create a message string
        output 
            message + S		"" + "appended data", big string that is used in classed Flash, Pulse and Combo
        '''
        S = b""
        tNow = time.time()

        #we try a number of times to actually get the next bit of data
        while len(S) == 0:
            S = self.socket.recv(4)
            if time.time() - tNow > 20:
                # it's been more than 20 seconds, and 
                # we haven't gotten a packet, not even a keepalive
                self.close_socket()
                self.whichIp = (self.whichIp+1)%len(self.ip)
                self.open_socket()
                return ""
        return message+S

            
    def run( self ):
        '''
        note: this method is not normally directly called.  Instead, this is initiated with 
        a ReedReceiver.start() call, where start() is inherited from threading.Thread

        input
            self
        '''
        if self.partnerId is None:
            raise Exception ( 'FeedReceiver.run : invalid Partner ID: %s'%repr(self.partnerId) )
        
        #set the running flag, we're officially running now!
        self.isRunning = True

        if self.format == 'ascii':
            # ascii feed
            if self.version == 2:
                self._loop_v2ascii()
            elif self.version == 3:
                self._loop_v3ascii()
            else:
                raise ValueError( "FeedReceiver.run: Unknown datafeed version: %s, not sure how to decode"%repr(self.version) )
        elif self.format == 'binary':
            # binary feed
            if self.version == 2:
                self._loop_v2binary()
            elif self.version == 3:
                self._loop_v3binary()
            else:
                raise ValueError( "FeedReceiver.run: Unknown datafeed version: %s, not sure how to decode"%repr(self.version) )
        else:
            raise ValueError("FeedReceiver.run: Unknown feed format:%i, not sure how to decode"%self.format)

    def stop( self ):
        self.isRunning = False
        self.close_socket()


########################
# Functions

def checksum( S ):
    """computes the checksum of ENTLN feed string
    """
    ###
    # computes the checksum as per EN documentation
    N = len(S)
    #this unpacks each byte
    l = struct.unpack( '%iB'%N, S )
    
    return (256-sum(l)%256)%256

def epoch2timestamp( t ):
    '''
    This method is used to convert the epoch to the Time stamp

    Parameters
    ----------
    :param t: Time value

    Returns
    -------
    :return: Time stamp value
    '''
    S = time.strftime( '%Y-%m-%dT%H:%M:%S', time.gmtime( t ) )
    return S

def timestamp2epochnano( timeStamp ):
    '''
    This method converts a timeStamp is a reasonable YearMonthDayHourMinuteSecond format into
    linux time (Seconds since 1970)
    Does this in a fairly flexible manner, maybe more so than needed

    Parameters
    ----------
    :param timeStamp: Timstamp String value


    Returns
    -------
    :returns Epoch seconds and epoch nano seconds
    '''
    #strip all non-numbers from the timeStamp
    strippedString = ''
    for c in timeStamp:
        if c.isdigit():
            strippedString += c

    #the first 14 numbers are for the date and time, the second 9 are the for fractional seconds
    if len(strippedString) < 14+9:
        #fill with 0's
        strippedString += '0'*(14+9-len(strippedString))

    #return epoch seconds, and nano seconds back
    return calendar.timegm( time.strptime( strippedString[:14], '%Y%m%d%H%M%S' ) ), int( strippedString[14:] )

