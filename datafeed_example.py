import FeedReceiver
import time #used for sleep

###############################################################################
#
# Copyright (c) 2017 Earth Networks, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

###############################################################################
#
# datafeed_example
# a minimal example of how to use the classes in FeedReceiver to establish 
# a connection to an Earth Networks lightning datafeed

# create the feed object (note <partner ID> needs to be filled in)
# basic optoins include:
#   partnerId=<partner ID>      - required for connection to work
#   ip=[ '<IP>', <IP>']         - a list of IPs to use for the connection
#   version= 2|3                - version 2 or version 3 feed connection
#   format = 'ascii'|'binary'   - format of lightning data in the feed
#   type   = 'flash'|'pulse'|'combo' - type of lightning returned in datafeed
# See implementation for more notes on options
feed = FeedReceiver.FeedReceiver( partnerId="<partner ID>", version=3, format='ascii', type='flash', decode=True )
# the FeedReceiver using Threading, and is fully asynchronous.  To start 
# getting data, you need to start the thread (as per standard)
feed.start()

# enter main loop
# each iteration, check to see if the FeedReceiver has collected any data, 
# if it has, pop it off and print something to standard out
while True:
    if len(feed.received ) == 0:
        # no lightning yet
        # just wait for a bit, the FeedReceiver will continue collecting 
        # data in the background
        time.sleep(.1)
        continue

    #otherwise, we have some data
    while len(feed.received) > 0:

        #get the element at the top of the FeedReceiver
        lightning = feed.received.pop(0) 

        #The FeedReceiver can decode the messages into Flash and/or Pulse objects
        if feed.decode:
            #the FeedReceiver is set up do decode:
            #print the original message string received from the lightning datafeed
            print (repr( lightning.messageString ))
            #print the standardized output from the decoded Flash/Pulse object
            print (lightning.printout())
        else:
            #no decoding has been done, 
            #print the original message string received from the lightning datafeed
            print (repr( lightning ))
